# Bryan Paget

1206 - 261 Cooper St. Ottawa ON K2P 0G3 Canada

1 (613) 262-3087 

bryanpaget\@pm.me

## Work Experience

#### Statistics Canada (2019)

*Data Scientist*

------------------------------------------------------------------------

-   Built a Python machine learning pipeline for text classification.

-   Gave presentations on Git and machine learning methodology.

#### Cheetah Networks (2019)

*Machine Learning Consultant*

------------------------------------------------------------------------

-   Designed and implemented a streaming SVM algorithm for predicting
    network latency.

#### Friends of Canadian Broadcasting (2018  2019)

*Data Scientist*

------------------------------------------------------------------------

-   Advanced data mining to produce detailed financial reports with key
    performance indicators.

#### University of Ottawa (2018)

*Bioinformatics Programmer*

------------------------------------------------------------------------

-   Bioinformatics algorithm research and implementation in C++.

#### Advanced Symbolics (2018)

*AI Intern*

------------------------------------------------------------------------

-   Built a predictive model for the 2018 Ontario election using data
    from Canadian census and Twitter.

#### University of Ottawa (2017)

*Teaching Assistant*

------------------------------------------------------------------------

-   Led calculus tutorials.

-   Helped calculus and linear algebra students in the math help centre.

#### University of Ottawa (2015  2017)

*NLP Research Assistant*

------------------------------------------------------------------------

-   Made a text classification algorithm to distinguish between
    rhythmical and non-rhythmical poetry.

-   Paper publication and presentation in FLAIRS 29.

### Education

#### University of Ottawa (2019)

*MSc. Statistics*

------------------------------------------------------------------------

-   Thesis on the theory behind GANs (generative adversarial networks),
    including game theory, information theory and optimal transport.

-   Took courses in information theory, computational statistics,
    financial mathematics, data mining.

#### University of Ottawa (2017)

*BSc. Major: Statistics, Minor: Psychology*

------------------------------------------------------------------------

-   Took courses in probability, linear algebra, statistical learning
    theory, regression, object oriented programming.

-   I had a paper published in the 29th Florida Artificial
    Intelligence Research Society conference.

### Online Courses

#### Coursera

UC San Diego *Finding Hidden Messages in DNA (Bioinformatics I)*.

#### Udemy

(In Progress) *Docker and Kubernetes: The Complete Guide*.

### Programming Languages and Tools

| Languages  | Operating Systems  | Tools               |
|:-----------|:-------------------|:--------------------|
| Python     | Windows            | Visual Studio Code  |
| R          | macOS              | PyCharm             |
| Bash       | Ubuntu             | Docker              |
| LaTeX      | Fedora             | Scikit-learn        |
| SQL        | Debian             | Vim                 |
| SAS        | CentOS             | Emacs               |
| Scala      |                    | Git                 |
| Julia      |                    | Docker              |
| Haskell    |                    | Jupyter Lab         |

### Awards

#### 2018

Swartzen Memorial Scholarship. *For academic excellence at the graduate
level.*

#### 2014

Undergraduate Research Opportunity Program. *NLP Research Assistant.*
